﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	[StructLayout(LayoutKind.Sequential)]
	public struct TMouseInfo
	{
		int x, y;
		/* absolute position */
		int dx, dy;
		/* movement since last update in pixels */
		int cx, cy;
		/* cell coordinates in the root console */
		int dcx, dcy;
		/* movement since last update in console cells */
		bool lbutton;
		/* left button status */
		bool rbutton;
		/* right button status */
		bool mbutton;
		/* middle button status */
		bool lbutton_pressed;
		/* left button pressed event */
		bool rbutton_pressed;
		/* right button pressed event */
		bool mbutton_pressed;
		/* middle button pressed event */
		bool wheel_up;
		/* wheel up event */
		bool wheel_down;
		/* wheel down event */
	}

	public partial class TMouse
	{
		[DllImport("tcod", EntryPoint = "TCOD_mouse_show_cursor")]
		private static extern void ShowCursor(bool visible);

		[DllImport("tcod", EntryPoint = "TCOD_mouse_get_status")]
		private static extern TMouseInfo GetStatus();

		[DllImport("tcod", EntryPoint = "TCOD_mouse_is_cursor_visible")]
		private static extern bool IsCursorVisible();

		[DllImport("tcod", EntryPoint = "TCOD_mouse_move")]
		private static extern void Move(int x, int y);

		[DllImport("tcod", EntryPoint = "TCOD_mouse_includes_touch")]
		private static extern void SetIncludesTouch(bool enable);
	}
}

