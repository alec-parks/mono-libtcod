﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_random_t = IntPtr;

	/* dice roll */
	public struct TDice
	{
		int nb_rolls;
		int nb_faces;
		float multiplier;
		float addsub;
	}

	public partial class TRandom
	{
		[DllImport("tcod")]
		private static extern TCOD_random_t TCOD_random_get_instance();
		[DllImport("tcod")]
		private static extern TCOD_random_t TCOD_random_new(TRandomAlgo algo);
		[DllImport("tcod")]
		private static extern TCOD_random_t TCOD_random_save(TCOD_random_t mersenne);
		[DllImport("tcod")]
		private static extern void TCOD_random_restore(TCOD_random_t mersenne, TCOD_random_t backup);
		[DllImport("tcod")]
		private static extern TCOD_random_t TCOD_random_new_from_seed(TRandomAlgo algo, uint seed);
		[DllImport("tcod")]
		private static extern void TCOD_random_delete(TCOD_random_t mersenne);

		[DllImport("tcod")]
		private static extern void TCOD_random_set_distribution (TCOD_random_t mersenne, TDistribution distribution);

		[DllImport("tcod")]
		private static extern int TCOD_random_get_int (TCOD_random_t mersenne, int min, int max);
		[DllImport("tcod")]
		private static extern float TCOD_random_get_float (TCOD_random_t mersenne, float min, float max);
		[DllImport("tcod")]
		private static extern double TCOD_random_get_double (TCOD_random_t mersenne, double min, double max);

		[DllImport("tcod")]
		private static extern int TCOD_random_get_int_mean (TCOD_random_t mersenne, int min, int max, int mean);
		[DllImport("tcod")]
		private static extern float TCOD_random_get_float_mean (TCOD_random_t mersenne, float min, float max, float mean);
		[DllImport("tcod")]
		private static extern double TCOD_random_get_double_mean (TCOD_random_t mersenne, double min, double max, double mean);

		[DllImport("tcod", EntryPoint = "TCOD_random_dice_new")]
		public static extern TDice DiceNew(string s);
		[DllImport("tcod")]
		private static extern int TCOD_random_dice_roll (TCOD_random_t mersenne, TDice dice);
		[DllImport("tcod")]
		private static extern int TCOD_random_dice_roll_s (TCOD_random_t mersenne, string s);
	}
}

