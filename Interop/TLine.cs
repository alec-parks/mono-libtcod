﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	public partial class TLine
	{
		[StructLayout(LayoutKind.Sequential)]
		internal class TBresenhamData {
			int stepx;
			int stepy;
			int e;
			int deltax;
			int deltay;
			int origx; 
			int origy; 
			int destx; 
			int desty; 
		}

		[DllImport("tcod")]
		private static extern void TCOD_line_init_mt(int xFrom, int yFrom, int xTo, int yTo, TBresenhamData data);

		[DllImport("tcod")]
		private static extern bool TCOD_line_step_mt(ref int xCur, ref int yCur, TBresenhamData data);
	}
}

