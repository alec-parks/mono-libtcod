﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_image_t = IntPtr;
	using TCOD_console_t = IntPtr;

	public partial class TImage
	{
		[DllImport("tcod")]
		private static extern TCOD_image_t TCOD_image_new(int width, int height);

		[DllImport("tcod")]
		private static extern TCOD_image_t TCOD_image_from_console(TCOD_console_t console);

		[DllImport("tcod")]
		private static extern void TCOD_image_refresh_console(TCOD_image_t image, TCOD_console_t console);

		[DllImport("tcod")]
		private static extern TCOD_image_t TCOD_image_load(string filename);

		[DllImport("tcod")]
		private static extern void TCOD_image_clear(TCOD_image_t image, TColorData color);

		[DllImport("tcod")]
		private static extern void TCOD_image_invert(TCOD_image_t image);

		[DllImport("tcod")]
		private static extern void TCOD_image_hflip(TCOD_image_t image);

		[DllImport("tcod")]
		private static extern void TCOD_image_rotate90(TCOD_image_t image, int numRotations);

		[DllImport("tcod")]
		private static extern void TCOD_image_vflip(TCOD_image_t image);

		[DllImport("tcod")]
		private static extern void TCOD_image_scale(TCOD_image_t image, int neww, int newh);

		[DllImport("tcod")]
		private static extern void TCOD_image_save(TCOD_image_t image, string filename);

		[DllImport("tcod")]
		private static extern void TCOD_image_get_size(TCOD_image_t image, ref int w, ref int h);

		[DllImport("tcod")]
		private static extern TColorData TCOD_image_get_pixel(TCOD_image_t image, int x, int y);

		[DllImport("tcod")]
		private static extern int TCOD_image_get_alpha(TCOD_image_t image, int x, int y);

		[DllImport("tcod")]
		private static extern TColorData TCOD_image_get_mipmap_pixel(TCOD_image_t image, float x0, float y0, float x1, float y1);

		[DllImport("tcod")]
		private static extern void TCOD_image_put_pixel(TCOD_image_t image, int x, int y, TColorData col);

		[DllImport("tcod")]
		private static extern void TCOD_image_blit(TCOD_image_t image, TCOD_console_t console, float x, float y, TBackgroundFlag bkgnd_flag, float scalex, float scaley, float angle);

		[DllImport("tcod")]
		private static extern void TCOD_image_blit_rect(TCOD_image_t image, TCOD_console_t console, int x, int y, int w, int h, TBackgroundFlag bkgnd_flag);

		[DllImport("tcod")]
		private static extern void TCOD_image_blit_2x(TCOD_image_t image, TCOD_console_t dest, int dx, int dy, int sx, int sy, int w, int h);

		[DllImport("tcod")]
		private static extern void TCOD_image_delete(TCOD_image_t image);

		[DllImport("tcod")]
		private static extern void TCOD_image_set_key_color(TCOD_image_t image, TColorData key_color);

		[DllImport("tcod")]
		private static extern bool TCOD_image_is_pixel_transparent(TCOD_image_t image, int x, int y);
	}
}

