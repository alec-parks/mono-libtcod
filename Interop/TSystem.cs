﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	public partial class TSystem
	{
		[DllImport("tcod", EntryPoint = "TCOD_sys_wait_for_event")]
		public static extern TEvent WaitForEvent(TEvent eventMask, [Out] TKeyInfo key, [Out] TMouseInfo mouse, bool flush);

		[DllImport("tcod", EntryPoint = "TCOD_sys_check_for_event")]
		public static extern TEvent CheckForEvent(TEvent eventMask, [Out] TKeyInfo key, [Out] TMouseInfo mouse);

		[DllImport("tcod", EntryPoint = "TCOD_sys_clipboard_set")]
		public static extern void ClipboardSet(string value);

		[DllImport("tcod", EntryPoint = "TCOD_sys_clipboard_get")]
		public static extern string ClipboardGet();

		[DllImport("tcod", EntryPoint = "TCOD_sys_elapsed_milli")]
		public static extern uint ElapsedMilli();

		[DllImport("tcod", EntryPoint = "TCOD_sys_elapsed_seconds")]
		public static extern float ElapsedSeconds();

		[DllImport("tcod", EntryPoint = "TCOD_sys_sleep_milli")]
		public static extern void SleepMilli(uint val);

		[DllImport("tcod", EntryPoint = "TCOD_sys_save_screenshot")]
		public static extern void SaveScreenshot(string filename);

		[DllImport("tcod", EntryPoint = "TCOD_sys_force_fullscreen_resolution")]
		public static extern void ForceFullscreenResolution(int width, int height);

		[DllImport("tcod", EntryPoint = "TCOD_sys_set_renderer")]
		public static extern void SetRenderer(TRenderer renderer);

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_renderer")]
		public static extern TRenderer GetRenderer();

		[DllImport("tcod", EntryPoint = "TCOD_sys_set_fps")]
		public static extern void SetFPS(int val);

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_fps")]
		public static extern int GetFPS();

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_last_frame_length")]
		public static extern float GetLastFrameLength();

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_current_resolution")]
		public static extern void GetCurrentResolution(ref int w, ref int h);

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_fullscreen_offsets")]
		public static extern void GetFullscreenOffsets(ref int offx, ref int offy);

		[DllImport("tcod", EntryPoint = "TCOD_sys_update_char")]
		private static extern void UpdateChar(int asciiCode, int fontx, int fonty, IntPtr img, int x, int y);

		[DllImport("tcod", EntryPoint = "TCOD_sys_get_char_size")]
		public static extern void GetCharSize(ref int w, ref int h);
	}
}

