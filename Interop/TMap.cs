﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_map_t = IntPtr;

	public partial class TMap
	{
		[DllImport("tcod")]
		private static extern TCOD_map_t TCOD_map_new(int width, int height);

		[DllImport("tcod")]
		private static extern void TCOD_map_clear(TCOD_map_t map, bool transparent, bool walkable);

		[DllImport("tcod")]
		private static extern void TCOD_map_copy(TCOD_map_t source, TCOD_map_t dest);

		[DllImport("tcod")]
		private static extern void TCOD_map_set_properties(TCOD_map_t map, int x, int y, bool is_transparent, bool is_walkable);

		[DllImport("tcod")]
		private static extern void TCOD_map_delete(TCOD_map_t map);

		[DllImport("tcod")]
		private static extern void TCOD_map_compute_fov(TCOD_map_t map, int player_x, int player_y, int max_radius, bool light_walls, FovAlgorithm algo);

		[DllImport("tcod")]
		private static extern bool TCOD_map_is_in_fov(TCOD_map_t map, int x, int y);

		[DllImport("tcod")]
		private static extern void TCOD_map_set_in_fov(TCOD_map_t map, int x, int y, bool fov);

		[DllImport("tcod")]
		private static extern bool TCOD_map_is_transparent(TCOD_map_t map, int x, int y);

		[DllImport("tcod")]
		private static extern bool TCOD_map_is_walkable(TCOD_map_t map, int x, int y);

		[DllImport("tcod")]
		private static extern int TCOD_map_get_width(TCOD_map_t map);

		[DllImport("tcod")]
		private static extern int TCOD_map_get_height(TCOD_map_t map);

		[DllImport("tcod")]
		private static extern int TCOD_map_get_nb_cells(TCOD_map_t map);
	}
}

