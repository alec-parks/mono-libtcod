﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_noise_t = IntPtr;
	using TCOD_random_t = IntPtr;

	public partial class TNoise
	{
		[DllImport("tcod")]
		private static extern TCOD_noise_t TCOD_noise_new(int dimensions, float hurst, float lacunarity, TCOD_random_t random);

		[DllImport("tcod")]
		private static extern void TCOD_noise_set_type(TCOD_noise_t noise, TNoiseType type);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get_ex(TCOD_noise_t noise, float[] f, TNoiseType type);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get_fbm_ex(TCOD_noise_t noise, float[] f, float octaves, TNoiseType type);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get_turbulence_ex(TCOD_noise_t noise, float[] f, float octaves, TNoiseType type);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get(TCOD_noise_t noise, float[] f);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get_fbm(TCOD_noise_t noise, float[] f, float octaves);

		[DllImport("tcod")]
		private static extern float TCOD_noise_get_turbulence(TCOD_noise_t noise, float[] f, float octaves);
		/* delete the noise object */
		[DllImport("tcod")]
		private static extern void TCOD_noise_delete(TCOD_noise_t noise);
	}
}

