﻿using System;

namespace TCOD
{
	/* PRNG algorithms */
	public enum TRandomAlgo
	{
		MersenneTwister,
		ComplementaryMultiply
	}

	public enum TDistribution
	{
		Linear,
		Gaussian,
		GaussianRange,
		GaussianInverse,
		GaussianRangeInverse
	}
}

