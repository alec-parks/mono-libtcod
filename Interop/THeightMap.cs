﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_noise_t = IntPtr;
	using TCOD_random_t = IntPtr;

	[StructLayout(LayoutKind.Sequential)]
	internal class TCOD_heightmap_t {
		internal int w,h;
		IntPtr values; // float*
	}

	public partial class THeightMap
	{
		[DllImport("tcod")]
		private static extern TCOD_heightmap_t TCOD_heightmap_new(int w,int h);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_delete(TCOD_heightmap_t hm);

		[DllImport("tcod")]
		private static extern float TCOD_heightmap_get_value(TCOD_heightmap_t hm, int x, int y);
		[DllImport("tcod")]
		private static extern float TCOD_heightmap_get_interpolated_value(TCOD_heightmap_t hm, float x, float y);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_set_value(TCOD_heightmap_t hm, int x, int y, float value);
		[DllImport("tcod")]
		private static extern float TCOD_heightmap_get_slope(TCOD_heightmap_t hm, int x, int y);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_get_normal(TCOD_heightmap_t hm, float x, float y, [MarshalAs(UnmanagedType.LPArray, SizeConst = 3)] float[] n, float waterLevel);
		[DllImport("tcod")]
		private static extern int TCOD_heightmap_count_cells(TCOD_heightmap_t hm, float min, float max);
		[DllImport("tcod")]
		private static extern bool TCOD_heightmap_has_land_on_border(TCOD_heightmap_t hm, float waterLevel);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_get_minmax(TCOD_heightmap_t hm, ref float min, ref float max);

		[DllImport("tcod")]
		private static extern void TCOD_heightmap_copy(TCOD_heightmap_t hm_source,TCOD_heightmap_t hm_dest);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_add(TCOD_heightmap_t hm, float value);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_scale(TCOD_heightmap_t hm, float value);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_clamp(TCOD_heightmap_t hm, float min, float max);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_normalize(TCOD_heightmap_t hm, float min, float max);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_clear(TCOD_heightmap_t hm);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_lerp_hm(TCOD_heightmap_t hm1, TCOD_heightmap_t hm2, TCOD_heightmap_t hmres, float coef);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_add_hm(TCOD_heightmap_t hm1, TCOD_heightmap_t hm2, TCOD_heightmap_t hmres);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_multiply_hm(TCOD_heightmap_t hm1, TCOD_heightmap_t hm2, TCOD_heightmap_t hmres);

		[DllImport("tcod")]
		private static extern void TCOD_heightmap_add_hill(TCOD_heightmap_t hm, float hx, float hy, float hradius, float hheight);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_dig_hill(TCOD_heightmap_t hm, float hx, float hy, float hradius, float hheight);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_dig_bezier(TCOD_heightmap_t hm, [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] int[] px, [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] int[] py, float startRadius, float startDepth, float endRadius, float endDepth);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_rain_erosion(TCOD_heightmap_t hm, int nbDrops,float erosionCoef,float sedimentationCoef,TCOD_random_t rnd);
		/* [DllImport("tcod")]
		private static extern void TCOD_heightmap_heat_erosion(TCOD_heightmap_t *hm, int nbPass,float minSlope,float erosionCoef,float sedimentationCoef,TCOD_random_t rnd); */
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_kernel_transform(TCOD_heightmap_t hm, int kernelsize, int[] dx, int[] dy, float[] weight, float minLevel,float maxLevel);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_add_voronoi(TCOD_heightmap_t hm, int nbPoints, int nbCoef, float[] coef, TCOD_random_t rnd);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_mid_point_displacement(TCOD_heightmap_t hm, TCOD_random_t rnd, float roughness);
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_add_fbm(TCOD_heightmap_t hm, TCOD_noise_t noise,float mulx, float muly, float addx, float addy, float octaves, float delta, float scale); 
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_scale_fbm(TCOD_heightmap_t hm, TCOD_noise_t noise,float mulx, float muly, float addx, float addy, float octaves, float delta, float scale); 
		[DllImport("tcod")]
		private static extern void TCOD_heightmap_islandify(TCOD_heightmap_t hm, float seaLevel,TCOD_random_t rnd);
	}
}

