﻿using System;

namespace TCOD
{
	public enum TEvent {
		None=0,
		KeyPress=1,
		KeyRelease=2,
		Key=KeyPress|KeyRelease,
		MouseMove=4,
		MousePress=8,
		MouseRelease=16,
		Mouse=MouseMove|MousePress|MouseRelease,
		/* #ifdef TCOD_TOUCH_INPUT */
		FingerMove=32,
		FingerPress=64,
		FingerRelease=128,
		Finger=FingerMove|FingerPress|FingerRelease,
		/* #endif */
		Any=Key|Mouse|Finger,
	};
}

