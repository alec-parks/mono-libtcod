﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_console_t = System.IntPtr;

	public partial class TConsole
	{
		#region TCOD_console_init

		[DllImport("tcod", EntryPoint = "TCOD_console_init_root")]
		private static extern TCOD_console_t InitRoot(int w, int h, string title, bool fullscreen, TRenderer renderer);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_root")]
		private static extern TCOD_console_t GetRoot();

		[DllImport("tcod", EntryPoint = "TCOD_console_set_window_title")]
		public static extern void SetWindowTitle(string title);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_fullscreen")]
		public static extern void SetFullscreen(bool fullscreen);

		[DllImport("tcod", EntryPoint = "TCOD_console_is_fullscreen")]
		public static extern bool IsFullscreen();

		[DllImport("tcod", EntryPoint = "TCOD_console_is_window_closed")]
		public static extern bool IsWindowClosed();

		[DllImport("tcod", EntryPoint = "TCOD_console_has_mouse_focus")]
		public static extern bool HasMouseFocus();

		[DllImport("tcod", EntryPoint = "TCOD_console_is_active")]
		public static extern bool IsActive();

		#endregion

		#region TCOD_console_font

		[DllImport("tcod", EntryPoint = "TCOD_console_set_custom_font")]
		public static extern void SetCustomFont(string fontFile, TFontFlags flags, int nb_char_horiz, int nb_char_vertic);

		[DllImport("tcod", EntryPoint = "TCOD_console_map_ascii_code_to_font")]
		public static extern void MapASCIICodeToFont(int asciiCode, int fontCharX, int fontCharY);

		[DllImport("tcod", EntryPoint = "TCOD_console_map_ascii_codes_to_font")]
		public static extern void MapASCIICodesToFont(int asciiCode, int nbCodes, int fontCharX, int fontCharY);

		[DllImport("tcod", EntryPoint = "TCOD_console_map_string_to_font")]
		public static extern void MapStringToFont(string s, int fontCharX, int fontCharY);

		#endregion

		#region TCOD_console_write

		[DllImport("tcod", EntryPoint = "TCOD_console_set_dirty")]
		public static extern void SetDirty(int x, int y, int w, int h);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_default_background")]
		private static extern void SetDefaultBackground(TCOD_console_t con, TColorData col);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_default_foreground")]
		private static extern void SetDefaultForeground(TCOD_console_t con, TColorData col);

		[DllImport("tcod", EntryPoint = "TCOD_console_clear")]
		private static extern void Clear(TCOD_console_t con);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_char_background")]
		private static extern void SetCharBackground(TCOD_console_t con, int x, int y, TColorData col, TBackgroundFlag flag);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_char_foreground")]
		private static extern void SetCharForeground(TCOD_console_t con, int x, int y, TColorData col);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_char")]
		private static extern void SetChar(TCOD_console_t con, int x, int y, int c);

		[DllImport("tcod", EntryPoint = "TCOD_console_put_char")]
		private static extern void PutCharStd(TCOD_console_t con, int x, int y, int c, TBackgroundFlag flag);

		[DllImport("tcod", EntryPoint = "TCOD_console_put_char_ex")]
		private static extern void PutCharEx(TCOD_console_t con, int x, int y, int c, TColorData fore, TColorData back);

		#endregion


		[DllImport("tcod")]
		private static extern void TCOD_console_print(TCOD_console_t con, int x, int y, string fmt);

		[DllImport("tcod")]
		private static extern void TCOD_console_print_ex(TCOD_console_t con, int x, int y, TBackgroundFlag flag, TAlignment alignment, string fmt);

		#region TCOD_console_draw

		[DllImport("tcod", EntryPoint = "TCOD_console_rect")]
		private static extern void Rect(TCOD_console_t con, int x, int y, int w, int h, bool clear, TBackgroundFlag flag);

		[DllImport("tcod", EntryPoint = "TCOD_console_hline")]
		private static extern void HLine(TCOD_console_t con, int x, int y, int l, TBackgroundFlag flag);

		[DllImport("tcod", EntryPoint = "TCOD_console_vline")]
		private static extern void VLine(TCOD_console_t con, int x, int y, int l, TBackgroundFlag flag);

		[DllImport("tcod", EntryPoint = "TCOD_console_print_frame")]
		private static extern void Frame(TCOD_console_t con, int x, int y, int w, int h, bool empty, TBackgroundFlag flag, string fmt);

		#endregion

		#region TCOD_console_background

		[DllImport("tcod", EntryPoint = "TCOD_console_get_default_background")]
		private static extern TColorData GetDefaultBackground(TCOD_console_t con);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_default_foreground")]
		private static extern TColorData GetDefaultForeground(TCOD_console_t con);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_char_background")]
		private static extern TColorData GetCharBackground(TCOD_console_t con, int x, int y);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_char_foreground")]
		private static extern TColorData GetCharForeground(TCOD_console_t con, int x, int y);

		#endregion

		[DllImport("tcod", EntryPoint = "TCOD_console_get_char")]
		private static extern int GetChar(TCOD_console_t con, int x, int y);

		#region Fade

		[DllImport("tcod", EntryPoint = "TCOD_console_set_fade")]
		private static extern void SetFade(byte val, TColorData fade);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_fade")]
		public static extern byte GetFade();

		[DllImport("tcod", EntryPoint = "TCOD_console_get_fading_color")]
		private static extern TColorData GetFadingColorInt();

		#endregion

		[DllImport("tcod", EntryPoint = "TCOD_console_flush")]
		public static extern void Flush();


		[DllImport("tcod", EntryPoint = "TCOD_console_set_color_control")]
		private static extern void SetColorControl(TColorControl con, TColorData fore, TColorData back);

		[DllImport("tcod", EntryPoint = "TCOD_console_check_for_keypress")]
		public static extern TKeyInfo CheckForKeypress(int flags);

		[DllImport("tcod", EntryPoint = "TCOD_console_wait_for_keypress")]
		public static extern TKeyInfo WaitForKeypress(bool flush);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_keyboard_repeat")]
		public static extern void SetKeyboardRepeat(int initial_delay, int interval);

		[DllImport("tcod", EntryPoint = "TCOD_console_disable_keyboard_repeat")]
		public static extern void DisableKeyboardRepeat();

		[DllImport("tcod", EntryPoint = "TCOD_console_is_key_pressed")]
		public static extern bool IsKeyPressed(TKeycode key);

		/* ASCII paint file support */
		[DllImport("tcod", EntryPoint = "TCOD_console_from_file")]
		private static extern TCOD_console_t FromFile(string filename);

		[DllImport("tcod", EntryPoint = "TCOD_console_load_asc")]
		private static extern bool LoadASC(TCOD_console_t con, string filename);

		[DllImport("tcod", EntryPoint = "TCOD_console_load_apf")]
		private static extern bool LoadAPF(TCOD_console_t con, string filename);

		[DllImport("tcod", EntryPoint = "TCOD_console_save_asc")]
		private static extern bool SaveASC(TCOD_console_t con, string filename);

		[DllImport("tcod", EntryPoint = "TCOD_console_save_apf")]
		private static extern bool SaveAPF(TCOD_console_t con, string filename);

		[DllImport("tcod", EntryPoint = "TCOD_console_new")]
		private static extern TCOD_console_t CreateNew(int w, int h);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_width")]
		private static extern int GetWidth(TCOD_console_t con);

		[DllImport("tcod", EntryPoint = "TCOD_console_get_height")]
		private static extern int GetHeight(TCOD_console_t con);

		[DllImport("tcod", EntryPoint = "TCOD_console_set_key_color")]
		private static extern void SetKeyColor(TCOD_console_t con, TColorData col);

		[DllImport("tcod", EntryPoint = "TCOD_console_blit")]
		private static extern void Blit(TCOD_console_t src, int xSrc, int ySrc, int wSrc, int hSrc, TCOD_console_t dst, int xDst, int yDst, float foreground_alpha, float background_alpha);

		[DllImport("tcod", EntryPoint = "TCOD_console_delete")]
		private static extern void Delete(TCOD_console_t console);


		[DllImport("tcod", EntryPoint = "TCOD_console_credits")]
		public static extern void Credits();

		[DllImport("tcod", EntryPoint = "TCOD_console_credits_reset")]
		public static extern void CreditsReset();

		[DllImport("tcod", EntryPoint = "TCOD_console_credits_render")]
		public static extern bool CreditsRender(int x, int y, bool alpha);
	}
}

