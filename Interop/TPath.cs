﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	using TCOD_path_t = IntPtr;
	using TCOD_map_t = IntPtr;
	using TCOD_dijkstra_t = IntPtr;

	public abstract class TPathCommon
	{
		public delegate float CostFunc(int xFrom, int yFrom, int xTo, int yTo, IntPtr user_data);
	}

	public partial class TPath : TPathCommon
	{
		[DllImport("tcod")]
		private static extern TCOD_path_t TCOD_path_new_using_map(TCOD_map_t map, float diagonalCost);

		[DllImport("tcod")]
		private static extern TCOD_path_t TCOD_path_new_using_function(int map_width, int map_height, CostFunc func, IntPtr user_data, float diagonalCost);

		[DllImport("tcod")]
		private static extern bool TCOD_path_compute(TCOD_path_t path, int ox, int oy, int dx, int dy);

		[DllImport("tcod")]
		private static extern bool TCOD_path_walk(TCOD_path_t path, ref int x, ref int y, bool recalculate_when_needed);

		[DllImport("tcod")]
		private static extern bool TCOD_path_is_empty(TCOD_path_t path);

		[DllImport("tcod")]
		private static extern int TCOD_path_size(TCOD_path_t path);

		[DllImport("tcod")]
		private static extern void TCOD_path_reverse(TCOD_path_t path);

		[DllImport("tcod")]
		private static extern void TCOD_path_get(TCOD_path_t path, int index, ref int x, ref int y);

		[DllImport("tcod")]
		private static extern void TCOD_path_get_origin(TCOD_path_t path, ref int x, ref int y);

		[DllImport("tcod")]
		private static extern void TCOD_path_get_destination(TCOD_path_t path, ref int x, ref int y);

		[DllImport("tcod")]
		private static extern void TCOD_path_delete(TCOD_path_t path);
	}

	public partial class TPathDijkstra : TPathCommon
	{
		[DllImport("tcod")]
		private static extern TCOD_dijkstra_t TCOD_dijkstra_new(TCOD_map_t map, float diagonalCost);

		[DllImport("tcod")]
		private static extern TCOD_dijkstra_t TCOD_dijkstra_new_using_function(int map_width, int map_height, CostFunc func, IntPtr user_data, float diagonalCost);

		[DllImport("tcod")]
		private static extern void TCOD_dijkstra_compute(TCOD_dijkstra_t dijkstra, int root_x, int root_y);

		[DllImport("tcod")]
		private static extern float TCOD_dijkstra_get_distance(TCOD_dijkstra_t dijkstra, int x, int y);

		[DllImport("tcod")]
		private static extern bool TCOD_dijkstra_path_set(TCOD_dijkstra_t dijkstra, int x, int y);

		[DllImport("tcod")]
		private static extern bool TCOD_dijkstra_is_empty(TCOD_dijkstra_t path);

		[DllImport("tcod")]
		private static extern int TCOD_dijkstra_size(TCOD_dijkstra_t path);

		[DllImport("tcod")]
		private static extern void TCOD_dijkstra_reverse(TCOD_dijkstra_t path);

		[DllImport("tcod")]
		private static extern void TCOD_dijkstra_get(TCOD_dijkstra_t path, int index, ref int x, ref int y);

		[DllImport("tcod")]
		private static extern bool TCOD_dijkstra_path_walk(TCOD_dijkstra_t dijkstra, ref int x, ref int y);

		[DllImport("tcod")]
		private static extern void TCOD_dijkstra_delete(TCOD_dijkstra_t dijkstra);
	}
}

