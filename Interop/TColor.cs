﻿using System;
using System.Runtime.InteropServices;

namespace TCOD
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct TColorData
	{
		public byte r, g, b;
	}

	public partial class TColor
	{
		[DllImport("tcod")]
		private static extern TColorData TCOD_color_RGB(byte r, byte g, byte b);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_HSV(float h, float s, float v);
		/* basic operations */
		[DllImport("tcod")]
		private static extern bool TCOD_color_equals(TColorData c1, TColorData c2);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_add(TColorData c1, TColorData c2);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_subtract(TColorData c1, TColorData c2);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_multiply(TColorData c1, TColorData c2);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_multiply_scalar(TColorData c1, float value);

		[DllImport("tcod")]
		private static extern TColorData TCOD_color_lerp(TColorData c1, TColorData c2, float coef);

		[DllImport("tcod")]
		private static extern void TCOD_color_set_HSV(ref TColorData c, float h, float s, float v);

		[DllImport("tcod")]
		private static extern void TCOD_color_get_HSV(TColorData c, ref float h, ref float s, ref float v);

		[DllImport("tcod")]
		private static extern float TCOD_color_get_hue(TColorData c);

		[DllImport("tcod")]
		private static extern void TCOD_color_set_hue(ref TColorData c, float h);

		[DllImport("tcod")]
		private static extern float TCOD_color_get_saturation(TColorData c);

		[DllImport("tcod")]
		private static extern void TCOD_color_set_saturation(ref TColorData c, float s);

		[DllImport("tcod")]
		private static extern float TCOD_color_get_value(TColorData c);

		[DllImport("tcod")]
		private static extern void TCOD_color_set_value(ref TColorData c, float v);

		[DllImport("tcod")]
		private static extern void TCOD_color_shift_hue(ref TColorData c, float hshift);

		[DllImport("tcod")]
		private static extern void TCOD_color_scale_HSV(ref TColorData c, float scoef, float vcoef);

	}
}

