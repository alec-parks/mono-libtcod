﻿using System;

namespace TCOD
{
	public enum FovAlgorithm
	{
		Basic,
		Diamond,
		Shadow,
		Permissive0,
		Permissive1,
		Permissive2,
		Permissive3,
		Permissive4,
		Permissive5,
		Permissive6,
		Permissive7,
		Permissive8,
		Restrictive
	}
}

