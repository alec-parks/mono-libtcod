﻿using System;

namespace TCOD
{
	public enum TNoiseType
	{
		Perlin = 1,
		Simplex = 2,
		Wavelet = 4,
		Default = 0
	}
}

