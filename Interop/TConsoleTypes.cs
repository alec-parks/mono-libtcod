﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace TCOD
{
	public enum TRenderer
	{
		GLSL,
		OPENGL,
		SDL

	}

	public enum TBackgroundFlag
	{
		None,
		Set,
		Multiply,
		Lighten,
		Darken,
		Screen,
		ColorDodge,
		ColorBurn,
		Add,
		AddAlpha,
		Burn,
		Overlay,
		Alpha,
		Default

	}

	public enum TColorControl
	{
		TCOD_COLCTRL_1 = 1,
		TCOD_COLCTRL_2,
		TCOD_COLCTRL_3,
		TCOD_COLCTRL_4,
		TCOD_COLCTRL_5,
		TCOD_COLCTRL_NUMBER = 5,
		TCOD_COLCTRL_FORE_RGB,
		TCOD_COLCTRL_BACK_RGB,
		TCOD_COLCTRL_STOP

	}

	public enum TKeycode
	{
		None,
		Escape,
		Backspace,
		Tab,
		Enter,
		Shift,
		Control,
		Alt,
		Pause,
		CapsLock,
		PageUp,
		PageDown,
		End,
		Home,
		Up,
		Left,
		Right,
		Down,
		PrintScreen,
		Insert,
		Delete,
		LWin,
		RWin,
		Apps,
		Key0,
		Key1,
		Key2,
		Key3,
		Key4,
		Key5,
		Key6,
		Key7,
		Key8,
		Key9,
		Num0,
		Num1,
		Num2,
		Num3,
		Num4,
		Num5,
		Num6,
		Num7,
		Num8,
		Num9,
		NumAdd,
		NumSub,
		NumDiv,
		NumMul,
		NumDec,
		NumEnter,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		NumLock,
		ScrollLock,
		Space,
		Char,
		Text

	}

	public enum TAlignment {
		Left, 
		Right, 
		Center 
	}


	public enum TFontFlags {
		LayoutColumn=1,
		LayoutRow=2,
		Grayscale=4,
		LayoutTcod=8,
	}

	[StructLayout(LayoutKind.Sequential)]
	public class TKeyInfo
	{
		public TKeycode vk;
		/*  key code */
		public char c;
		/* character if vk == TCODK_CHAR else 0 */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
		public byte[] text = new byte[32];
		/* text if vk == TCODK_TEXT else text[0] == '\0' */
		public bool pressed;
		/* does this correspond to a key press or key release event ? */
		public bool lalt;
		public bool lctrl;
		public bool lmeta;
		public bool ralt;
		public bool rctrl;
		public bool rmeta;
		public bool shift;

		public override string ToString() {
			var sb = new StringBuilder();
			sb.AppendFormat("TKeyInfo({0}", vk);
			if (vk == TKeycode.Char) {
				sb.AppendFormat(", '{0}'", c);
			}
			if (shift)
				sb.Append(", Shift");
			if (lalt)
				sb.Append(", Alt");
			if (ralt)
				sb.Append(", Alt_R");
			if (lctrl)
				sb.Append(", Ctrl");
			if (rctrl)
				sb.Append(", Ctrl_R");
			if (lmeta)
				sb.Append(", Meta");
			if (rmeta)
				sb.Append(", Meta_R");
			sb.Append(")");
			return sb.ToString();
		}
	}
}

