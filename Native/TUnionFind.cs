﻿using System;

namespace TCOD
{
	public class TUnionFind {
		private int[] sz;
		private int[] id;

		public TUnionFind(int size) {
			this.sz = new int[size];
			this.id = new int[size];

			for (var i = 0; i < size; ++i) {
				this.sz[i] = 1;
				this.id[i] = i;
			}
		}

		public void Union(int p, int q) {
			var i = Find(p);
			var j = Find(q);

			if (i == j)
				return;

			if (this.sz[i] < this.sz[j]) {
				this.id[i] = j;
				this.sz[j] += this.sz[i];
			} else {
				this.id[j] = i;
				this.sz[i] += this.sz[j];
			}
		}

		public int Find(int p) {
			while(p != this.id[p]) {
				this.id[p] = this.id[this.id[p]];
				p = this.id[p];
			}

			return p;
		}
	}
}

