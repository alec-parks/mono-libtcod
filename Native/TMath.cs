﻿using System;

namespace TCOD
{
	public static class TMath
	{
		public static int Distance(int x1, int y1, int x2, int y2)
		{
			var dx = Abs(x1 - x2);
			var dy = Abs(y1 - y2);

			return (int)Math.Sqrt(dx * dx + dy * dy);
		}

		public static int Abs(int n)
		{
			return n & 0x7FFFFFFF;
		}
	}
}

