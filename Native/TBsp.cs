﻿using System;
using System.Collections.Generic;

namespace TCOD
{
	public class TBsp
	{
		public int x = 0, y = 0, w = 0, h = 0;
		public int position = 0;
		public int level = 0;
		public bool horizontal = false;

		public TBsp left = null;
		public TBsp right = null;

		public bool isLeaf {
			get { return left == null && right == null; }
		}

		public TBsp(TRect rect)
			: this(rect.origin.x, rect.origin.y, rect.size.w, rect.size.h) {
			
		}

		public TBsp(int x, int y, int w, int h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			this.level = 0;
		}

		private TBsp(TBsp parent, bool left) {
			if (parent.horizontal) {
				x = parent.x;
				w = parent.w;
				y = left ? parent.y : parent.position;
				h = left ? parent.position - y : parent.y + parent.h - parent.position;
			} else {
				y = parent.y;
				h = parent.h;
				x = left ? parent.x : parent.position;
				w = left ? parent.position - x : parent.x + parent.w - parent.position;
			}
			level = parent.level + 1;
		}

		private void SplitOnce(bool horizontal, int position) {
			this.horizontal = horizontal;
			this.position = position;
			this.left = new TBsp(this, true);
			this.right = new TBsp(this, false);
		}

		public void SplitRecursive(TRandom rng, int nb, int minHSize, int minVSize, float maxHRatio, float maxVRatio) {
			if (nb == 0 || (w < 2 * minHSize && h < 2 * minVSize))
				return;
			
			bool horiz;

			rng = rng ?? TRandom.GetInstance();

			// promote square rooms
			if (h < 2 * minVSize || w > h * maxHRatio)
				horiz = false;
			else if (w < 2 * minHSize || h > w * maxVRatio)
				horiz = true;
			else
				horiz = rng.GetInt(0, 1) == 0;
			int position;
			if (horiz) {
				position = rng.GetInt(y + minVSize, y + h - minVSize);
			} else {
				position = rng.GetInt(x + minHSize, x + w - minHSize);
			}
			SplitOnce(horiz, position);
			left.SplitRecursive(rng, nb - 1, minHSize, minVSize, maxHRatio, maxVRatio);
			right.SplitRecursive(rng, nb - 1, minHSize, minVSize, maxHRatio, maxVRatio);
		}

		public void Resize(int x, int y, int w, int h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			if (left != null) {
				if (horizontal) {
					left.Resize(x, y, w, position - y);
					right.Resize(x, position, w, y + h - position);
				} else {
					left.Resize(x, y, position - x, h);
					right.Resize(position, y, x + w - position, h);
				}
			}
		}

		public bool Contains(int px, int py) {
			return (px >= x && py >= y && px < x + w && py < y + h);
		}


		public TBsp FindNode(int px, int py) {
			if (!Contains(px, py))
				return null;
			if (!isLeaf) {
				if (left.Contains(px, py))
					return left.FindNode(px, py);
				
				if (right.Contains(px, py))
					return right.FindNode(px, py);
			}
			return this;
		}

		public TPoint GetCenter() {
			return new TPoint {
				x = x + (w / 2),
				y = y + (w / 2)
			};
		}

		public TBinaryTreeVisitor<TBsp> GetVisitor() {
			return new TBspVisitor(this);
		}
	}

	internal class TBspVisitor : TBinaryTreeVisitor<TBsp>
	{
		private TBsp root;

		internal TBspVisitor(TBsp root) {
			this.root = root;
		}

		public IEnumerable<TBsp> PreOrder() {
			Stack<TBsp> stack = new Stack<TBsp>();
			stack.Push(root);

			while (stack.Count > 0) {
				TBsp node = stack.Pop();

				yield return node;

				if (node.left != null)
					stack.Push(node.left);
				if (node.right != null)
					stack.Push(node.right);
			}
		}

		public IEnumerable<TBsp> InOrder() {
			Stack<TBsp> stack = new Stack<TBsp>();
			TBsp current = null;
			bool done = false;

			while (!done) {
				if (current != null) {
					stack.Push(current);
					current = current.left;
				} else {
					if (stack.Count > 0) {
						current = stack.Pop();

						yield return current;

						current = current.right;
					} else {
						done = true;
					}
				}
			}
		}

		public IEnumerable<TBsp> PostOrder() {
			Stack<TBsp> stack = new Stack<TBsp>();
			TBsp cur = root;
			do {

				while (cur != null) {
					if (cur.right != null)
						stack.Push(cur.right);
					stack.Push(cur);

					cur = cur.left;
				}

				cur = stack.Pop();

				if (cur.right != null && stack.Peek() != cur.right) {
					stack.Pop();
					stack.Push(cur);
					cur = cur.right;
				} else {
					yield return cur;
					cur = null;
				}

			} while(stack.Count > 0);
		}

		public IEnumerable<TBsp> BreadthFirst() {
			Queue<TBsp> queue = new Queue<TBsp>();
			queue.Enqueue(root);

			while (queue.Count > 0) {
				var cur = queue.Dequeue();
				yield return cur;

				if (cur.left != null)
					queue.Enqueue(cur.left);
				if (cur.right != null)
					queue.Enqueue(cur.right);
			}
		}

		public IEnumerable<TBsp> DepthFirst() {
			Queue<TBsp> queue = new Queue<TBsp>();
			Stack<TBsp> stack = new Stack<TBsp>();

			queue.Enqueue(root);

			while(queue.Count > 0) {
				var cur = queue.Dequeue();
				stack.Push(cur);

				if (cur.right != null)
					queue.Enqueue(cur.right);
				if (cur.left != null)
					queue.Enqueue(cur.left);
			}

			while(stack.Count > 0) {
				var cur = stack.Pop();

				yield return cur;
			}
		}
	}
}

