﻿using System;

namespace TCOD
{
	[Serializable]
	public struct TPoint
	{
		public static readonly TPoint Zero = new TPoint(0, 0);

		public static readonly TPoint North = new TPoint(0, -1);
		public static readonly TPoint South = new TPoint(0, 1);
		public static readonly TPoint East = new TPoint(1, 0);
		public static readonly TPoint West = new TPoint(-1, 0);

		public static readonly TPoint[] Cardinals = new TPoint[] {
			East, North, West, South
		};

		public int x, y;

		public TPoint(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public TPoint Add(int xd, int yd) {
			return new TPoint(x + xd, y + yd);
		}

		public TPoint Add(TPoint o) {
			return new TPoint(x + o.x, y + o.y);
		}

		public TPoint Sub(TPoint o) {
			return new TPoint(x - o.x, y - o.y);
		}

		public TPoint Mul(int xm) {
			return new TPoint(x * xm, y * xm);
		}

		public int DistanceTo(TPoint other) {
			return TMath.Distance(x, y, other.x, other.y);
		}

		public override string ToString() {
			return string.Format("TPoint({0}, {1})", x, y);
		}

		public static bool operator==(TPoint p1, TPoint p2) {
			return p1.x == p2.x && p1.y == p2.y;
		}

		public static bool operator!=(TPoint p1, TPoint p2) {
			return !(p1 == p2);
		}
	}

	[Serializable]
	public struct TSize
	{
		public static readonly TSize Zero = new TSize(0, 0);

		public int w, h;

		public TSize(int w, int h) {
			this.w = w;
			this.h = h;
		}

		public TSize Grow(int wd, int hd) {
			return new TSize(w + wd, h + hd);
		}

		public TSize Mul(int xm) {
			return new TSize(w * xm, h * xm);
		}

		public int Surface {
			get { return w * h; }
		}

		public override string ToString() {
			return string.Format("TSize({0}, {1})", w, h);
		}
	}

	[Serializable]
	public struct TRect
	{
		public static readonly TRect Zero = new TRect(TPoint.Zero, TSize.Zero);

		public TPoint origin;
		public TSize size;

		public TPoint opposite {
			get { return new TPoint(origin.x + size.w, origin.y + size.h); }
		}

		public TRect(TPoint origin, TSize size) {
			this.origin = origin;
			this.size = size;
		}

		public TRect(TSize size) {
			this.origin = TPoint.Zero;
			this.size = size;
		}

		public TRect(int x, int y, int w, int h) {
			this.origin = new TPoint(x, y);
			this.size = new TSize(w, h);
		}

		public TRect Move(int xd, int yd) {
			return new TRect(origin.Add(xd, yd), size);
		}

		public TRect Grow(int wd, int hd) {
			return new TRect(origin, size.Grow(wd, hd));
		}

		public TRect Margins(int vertical = 0, int horizontal = 0) {
			return new TRect(
				origin.x + horizontal, origin.y + vertical,
				size.w - 2 * horizontal, size.h - 2 * vertical
			);
		}

		public TRect Margins(int top = 0, int right = 0, int bottom = 0, int left = 0) {
			return new TRect(
				origin.x + left, origin.y + top,
				size.w - left - right, size.h - top - bottom
			);
		}

		public int x1 { get { return this.origin.x; } }

		public int x2 { get { return this.origin.x + this.size.w; } }

		public int y1 { get { return this.origin.y; } }

		public int y2 { get { return this.origin.y + this.size.h; } }

		public bool Contains(TPoint p) {
			return this.x1 <= p.x && p.x <= this.x2 && this.y1 <= p.y && p.y <= this.y2;
		}

		public bool Intersects(TRect other) {
			/*
            if (RectA.Left < RectB.Right && RectA.Right > RectB.Left &&
                RectA.Bottom < RectB.Top && RectA.Top > RectB.Bottom)
            */
			return this.x1 < other.x2
			&& this.x2 > other.x1
			&& this.y1 < other.y2
			&& this.y2 > other.y1;
		}

		public override string ToString() {
			return string.Format("TRect(origin = {0}, size = {1})", origin, size);
		}
	}
}

