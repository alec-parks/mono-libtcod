﻿using System;
using System.Collections.Generic;

namespace TCOD
{
	public interface TTreeVisitor<TNode>
	{
		IEnumerable<TNode> PreOrder();

		IEnumerable<TNode> PostOrder();

		IEnumerable<TNode> BreadthFirst();

		IEnumerable<TNode> DepthFirst();
	}

	public interface TBinaryTreeVisitor<TNode> : TTreeVisitor<TNode>
	{
		IEnumerable<TNode> InOrder();
	}

	public class TTree<TData>
	{
		public TData Data {
			get;
			private set;
		}

		public IList<TTree<TData>> Children {
			get;
			private set;
		}

		public TTree(TData data) {
			Data = data;
			Children = new List<TTree<TData>>();
		}

		public void AddChild(TTree<TData> node) {
			Children.Add(node);
		}
	}
}

