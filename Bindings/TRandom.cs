﻿using System;
using System.Collections.Generic;

namespace TCOD
{
	public partial class TRandom
	{
		private static TRandom instance;
		internal IntPtr ptr;

		public static TRandom GetInstance() {
			if (instance == null) {
				var data = TCOD_random_get_instance();
				instance = new TRandom(data);
			}
			return instance;
		}

		public TRandom(TRandomAlgo algo) {
			ptr = TCOD_random_new(algo);
		}

		public TRandom(uint seed, TRandomAlgo algo) {
			ptr = TCOD_random_new_from_seed(algo, seed);
		}

		private TRandom(IntPtr ptr) {
			this.ptr = ptr;
		}

		~TRandom() {
			TCOD_random_delete(ptr);
		}

		public TRandom Save() {
			var data = TCOD_random_save(ptr);
			return new TRandom(data);
		}

		public void Restore(TRandom backup) {
			TCOD_random_restore(ptr, backup.ptr);
		}

		public void SetDistribution (TDistribution distribution) {
			TCOD_random_set_distribution(ptr, distribution);
		}

		public int GetInt (int min, int max) {
			return TCOD_random_get_int(ptr, min, max);
		}

		public float GetFloat (float min, float max) {
			return TCOD_random_get_float(ptr, min, max);
		}

		public double GetDouble(double min, double max) {
			return TCOD_random_get_double(ptr, min, max);
		}

		public int GetIntMean (int min, int max, int mean) {
			return TCOD_random_get_int_mean(ptr, min, max, mean);
		}

		public float GetFloatMean (float min, float max, float mean) {
			return TCOD_random_get_float_mean(ptr, min, max, mean);
		}

		public double GetDoubleMean (double min, double max, double mean) {
			return TCOD_random_get_double_mean(ptr, min, max, mean);
		}

		public bool GetBool(float chance) {
			return GetFloat(0, 1) < chance;
		}

		public TVal PickOne<TVal>(List<TVal> values) {
			if (values.Count == 0)
				return default(TVal);

			return values[GetInt(0, values.Count - 1)];
		}

		public int DiceRoll (TDice dice) {
			return TCOD_random_dice_roll(ptr, dice);
		}

		public int RiceRoll (string s) {
			return TCOD_random_dice_roll_s(ptr, s);
		}
	}
}

