﻿using System;

namespace TCOD
{
	public static class TColors
	{
		/* color values */
		public  static readonly TColor BLACK = new TColor(0, 0, 0);
		public  static readonly TColor DARKEST_GREY = new TColor(31, 31, 31);
		public  static readonly TColor DARKER_GREY = new TColor(63, 63, 63);
		public  static readonly TColor DARK_GREY = new TColor(95, 95, 95);
		public  static readonly TColor GREY = new TColor(127, 127, 127);
		public  static readonly TColor LIGHT_GREY = new TColor(159, 159, 159);
		public  static readonly TColor LIGHTER_GREY = new TColor(191, 191, 191);
		public  static readonly TColor LIGHTEST_GREY = new TColor(223, 223, 223);
		public  static readonly TColor WHITE = new TColor(255, 255, 255);

		public  static readonly TColor DARKEST_SEPIA = new TColor(31, 24, 15);
		public  static readonly TColor DARKER_SEPIA = new TColor(63, 50, 31);
		public  static readonly TColor DARK_SEPIA = new TColor(94, 75, 47);
		public  static readonly TColor SEPIA = new TColor(127, 101, 63);
		public  static readonly TColor LIGHT_SEPIA = new TColor(158, 134, 100);
		public  static readonly TColor LIGHTER_SEPIA = new TColor(191, 171, 143);
		public  static readonly TColor LIGHTEST_SEPIA = new TColor(222, 211, 195);

		/* desaturated */
		public  static readonly TColor DESATURATED_RED = new TColor(127, 63, 63);
		public  static readonly TColor DESATURATED_FLAME = new TColor(127, 79, 63);
		public  static readonly TColor DESATURATED_ORANGE = new TColor(127, 95, 63);
		public  static readonly TColor DESATURATED_AMBER = new TColor(127, 111, 63);
		public  static readonly TColor DESATURATED_YELLOW = new TColor(127, 127, 63);
		public  static readonly TColor DESATURATED_LIME = new TColor(111, 127, 63);
		public  static readonly TColor DESATURATED_CHARTREUSE = new TColor(95, 127, 63);
		public  static readonly TColor DESATURATED_GREEN = new TColor(63, 127, 63);
		public  static readonly TColor DESATURATED_SEA = new TColor(63, 127, 95);
		public  static readonly TColor DESATURATED_TURQUOISE = new TColor(63, 127, 111);
		public  static readonly TColor DESATURATED_CYAN = new TColor(63, 127, 127);
		public  static readonly TColor DESATURATED_SKY = new TColor(63, 111, 127);
		public  static readonly TColor DESATURATED_AZURE = new TColor(63, 95, 127);
		public  static readonly TColor DESATURATED_BLUE = new TColor(63, 63, 127);
		public  static readonly TColor DESATURATED_HAN = new TColor(79, 63, 127);
		public  static readonly TColor DESATURATED_VIOLET = new TColor(95, 63, 127);
		public  static readonly TColor DESATURATED_PURPLE = new TColor(111, 63, 127);
		public  static readonly TColor DESATURATED_FUCHSIA = new TColor(127, 63, 127);
		public  static readonly TColor DESATURATED_MAGENTA = new TColor(127, 63, 111);
		public  static readonly TColor DESATURATED_PINK = new TColor(127, 63, 95);
		public  static readonly TColor DESATURATED_CRIMSON = new TColor(127, 63, 79);

		/* lightest */
		public  static readonly TColor LIGHTEST_RED = new TColor(255, 191, 191);
		public  static readonly TColor LIGHTEST_FLAME = new TColor(255, 207, 191);
		public  static readonly TColor LIGHTEST_ORANGE = new TColor(255, 223, 191);
		public  static readonly TColor LIGHTEST_AMBER = new TColor(255, 239, 191);
		public  static readonly TColor LIGHTEST_YELLOW = new TColor(255, 255, 191);
		public  static readonly TColor LIGHTEST_LIME = new TColor(239, 255, 191);
		public  static readonly TColor LIGHTEST_CHARTREUSE = new TColor(223, 255, 191);
		public  static readonly TColor LIGHTEST_GREEN = new TColor(191, 255, 191);
		public  static readonly TColor LIGHTEST_SEA = new TColor(191, 255, 223);
		public  static readonly TColor LIGHTEST_TURQUOISE = new TColor(191, 255, 239);
		public  static readonly TColor LIGHTEST_CYAN = new TColor(191, 255, 255);
		public  static readonly TColor LIGHTEST_SKY = new TColor(191, 239, 255);
		public  static readonly TColor LIGHTEST_AZURE = new TColor(191, 223, 255);
		public  static readonly TColor LIGHTEST_BLUE = new TColor(191, 191, 255);
		public  static readonly TColor LIGHTEST_HAN = new TColor(207, 191, 255);
		public  static readonly TColor LIGHTEST_VIOLET = new TColor(223, 191, 255);
		public  static readonly TColor LIGHTEST_PURPLE = new TColor(239, 191, 255);
		public  static readonly TColor LIGHTEST_FUCHSIA = new TColor(255, 191, 255);
		public  static readonly TColor LIGHTEST_MAGENTA = new TColor(255, 191, 239);
		public  static readonly TColor LIGHTEST_PINK = new TColor(255, 191, 223);
		public  static readonly TColor LIGHTEST_CRIMSON = new TColor(255, 191, 207);

		/* lighter */
		public  static readonly TColor LIGHTER_RED = new TColor(255, 127, 127);
		public  static readonly TColor LIGHTER_FLAME = new TColor(255, 159, 127);
		public  static readonly TColor LIGHTER_ORANGE = new TColor(255, 191, 127);
		public  static readonly TColor LIGHTER_AMBER = new TColor(255, 223, 127);
		public  static readonly TColor LIGHTER_YELLOW = new TColor(255, 255, 127);
		public  static readonly TColor LIGHTER_LIME = new TColor(223, 255, 127);
		public  static readonly TColor LIGHTER_CHARTREUSE = new TColor(191, 255, 127);
		public  static readonly TColor LIGHTER_GREEN = new TColor(127, 255, 127);
		public  static readonly TColor LIGHTER_SEA = new TColor(127, 255, 191);
		public  static readonly TColor LIGHTER_TURQUOISE = new TColor(127, 255, 223);
		public  static readonly TColor LIGHTER_CYAN = new TColor(127, 255, 255);
		public  static readonly TColor LIGHTER_SKY = new TColor(127, 223, 255);
		public  static readonly TColor LIGHTER_AZURE = new TColor(127, 191, 255);
		public  static readonly TColor LIGHTER_BLUE = new TColor(127, 127, 255);
		public  static readonly TColor LIGHTER_HAN = new TColor(159, 127, 255);
		public  static readonly TColor LIGHTER_VIOLET = new TColor(191, 127, 255);
		public  static readonly TColor LIGHTER_PURPLE = new TColor(223, 127, 255);
		public  static readonly TColor LIGHTER_FUCHSIA = new TColor(255, 127, 255);
		public  static readonly TColor LIGHTER_MAGENTA = new TColor(255, 127, 223);
		public  static readonly TColor LIGHTER_PINK = new TColor(255, 127, 191);
		public  static readonly TColor LIGHTER_CRIMSON = new TColor(255, 127, 159);

		/* light */
		public  static readonly TColor LIGHT_RED = new TColor(255, 63, 63);
		public  static readonly TColor LIGHT_FLAME = new TColor(255, 111, 63);
		public  static readonly TColor LIGHT_ORANGE = new TColor(255, 159, 63);
		public  static readonly TColor LIGHT_AMBER = new TColor(255, 207, 63);
		public  static readonly TColor LIGHT_YELLOW = new TColor(255, 255, 63);
		public  static readonly TColor LIGHT_LIME = new TColor(207, 255, 63);
		public  static readonly TColor LIGHT_CHARTREUSE = new TColor(159, 255, 63);
		public  static readonly TColor LIGHT_GREEN = new TColor(63, 255, 63);
		public  static readonly TColor LIGHT_SEA = new TColor(63, 255, 159);
		public  static readonly TColor LIGHT_TURQUOISE = new TColor(63, 255, 207);
		public  static readonly TColor LIGHT_CYAN = new TColor(63, 255, 255);
		public  static readonly TColor LIGHT_SKY = new TColor(63, 207, 255);
		public  static readonly TColor LIGHT_AZURE = new TColor(63, 159, 255);
		public  static readonly TColor LIGHT_BLUE = new TColor(63, 63, 255);
		public  static readonly TColor LIGHT_HAN = new TColor(111, 63, 255);
		public  static readonly TColor LIGHT_VIOLET = new TColor(159, 63, 255);
		public  static readonly TColor LIGHT_PURPLE = new TColor(207, 63, 255);
		public  static readonly TColor LIGHT_FUCHSIA = new TColor(255, 63, 255);
		public  static readonly TColor LIGHT_MAGENTA = new TColor(255, 63, 207);
		public  static readonly TColor LIGHT_PINK = new TColor(255, 63, 159);
		public  static readonly TColor LIGHT_CRIMSON = new TColor(255, 63, 111);

		/* normal */
		public  static readonly TColor RED = new TColor(255, 0, 0);
		public  static readonly TColor FLAME = new TColor(255, 63, 0);
		public  static readonly TColor ORANGE = new TColor(255, 127, 0);
		public  static readonly TColor AMBER = new TColor(255, 191, 0);
		public  static readonly TColor YELLOW = new TColor(255, 255, 0);
		public  static readonly TColor LIME = new TColor(191, 255, 0);
		public  static readonly TColor CHARTREUSE = new TColor(127, 255, 0);
		public  static readonly TColor GREEN = new TColor(0, 255, 0);
		public  static readonly TColor SEA = new TColor(0, 255, 127);
		public  static readonly TColor TURQUOISE = new TColor(0, 255, 191);
		public  static readonly TColor CYAN = new TColor(0, 255, 255);
		public  static readonly TColor SKY = new TColor(0, 191, 255);
		public  static readonly TColor AZURE = new TColor(0, 127, 255);
		public  static readonly TColor BLUE = new TColor(0, 0, 255);
		public  static readonly TColor HAN = new TColor(63, 0, 255);
		public  static readonly TColor VIOLET = new TColor(127, 0, 255);
		public  static readonly TColor PURPLE = new TColor(191, 0, 255);
		public  static readonly TColor FUCHSIA = new TColor(255, 0, 255);
		public  static readonly TColor MAGENTA = new TColor(255, 0, 191);
		public  static readonly TColor PINK = new TColor(255, 0, 127);
		public  static readonly TColor CRIMSON = new TColor(255, 0, 63);

		/* dark */
		public  static readonly TColor DARK_RED = new TColor(191, 0, 0);
		public  static readonly TColor DARK_FLAME = new TColor(191, 47, 0);
		public  static readonly TColor DARK_ORANGE = new TColor(191, 95, 0);
		public  static readonly TColor DARK_AMBER = new TColor(191, 143, 0);
		public  static readonly TColor DARK_YELLOW = new TColor(191, 191, 0);
		public  static readonly TColor DARK_LIME = new TColor(143, 191, 0);
		public  static readonly TColor DARK_CHARTREUSE = new TColor(95, 191, 0);
		public  static readonly TColor DARK_GREEN = new TColor(0, 191, 0);
		public  static readonly TColor DARK_SEA = new TColor(0, 191, 95);
		public  static readonly TColor DARK_TURQUOISE = new TColor(0, 191, 143);
		public  static readonly TColor DARK_CYAN = new TColor(0, 191, 191);
		public  static readonly TColor DARK_SKY = new TColor(0, 143, 191);
		public  static readonly TColor DARK_AZURE = new TColor(0, 95, 191);
		public  static readonly TColor DARK_BLUE = new TColor(0, 0, 191);
		public  static readonly TColor DARK_HAN = new TColor(47, 0, 191);
		public  static readonly TColor DARK_VIOLET = new TColor(95, 0, 191);
		public  static readonly TColor DARK_PURPLE = new TColor(143, 0, 191);
		public  static readonly TColor DARK_FUCHSIA = new TColor(191, 0, 191);
		public  static readonly TColor DARK_MAGENTA = new TColor(191, 0, 143);
		public  static readonly TColor DARK_PINK = new TColor(191, 0, 95);
		public  static readonly TColor DARK_CRIMSON = new TColor(191, 0, 47);

		/* darker */
		public  static readonly TColor DARKER_RED = new TColor(127, 0, 0);
		public  static readonly TColor DARKER_FLAME = new TColor(127, 31, 0);
		public  static readonly TColor DARKER_ORANGE = new TColor(127, 63, 0);
		public  static readonly TColor DARKER_AMBER = new TColor(127, 95, 0);
		public  static readonly TColor DARKER_YELLOW = new TColor(127, 127, 0);
		public  static readonly TColor DARKER_LIME = new TColor(95, 127, 0);
		public  static readonly TColor DARKER_CHARTREUSE = new TColor(63, 127, 0);
		public  static readonly TColor DARKER_GREEN = new TColor(0, 127, 0);
		public  static readonly TColor DARKER_SEA = new TColor(0, 127, 63);
		public  static readonly TColor DARKER_TURQUOISE = new TColor(0, 127, 95);
		public  static readonly TColor DARKER_CYAN = new TColor(0, 127, 127);
		public  static readonly TColor DARKER_SKY = new TColor(0, 95, 127);
		public  static readonly TColor DARKER_AZURE = new TColor(0, 63, 127);
		public  static readonly TColor DARKER_BLUE = new TColor(0, 0, 127);
		public  static readonly TColor DARKER_HAN = new TColor(31, 0, 127);
		public  static readonly TColor DARKER_VIOLET = new TColor(63, 0, 127);
		public  static readonly TColor DARKER_PURPLE = new TColor(95, 0, 127);
		public  static readonly TColor DARKER_FUCHSIA = new TColor(127, 0, 127);
		public  static readonly TColor DARKER_MAGENTA = new TColor(127, 0, 95);
		public  static readonly TColor DARKER_PINK = new TColor(127, 0, 63);
		public  static readonly TColor DARKER_CRIMSON = new TColor(127, 0, 31);

		/* darkest */
		public  static readonly TColor DARKEST_RED = new TColor(63, 0, 0);
		public  static readonly TColor DARKEST_FLAME = new TColor(63, 15, 0);
		public  static readonly TColor DARKEST_ORANGE = new TColor(63, 31, 0);
		public  static readonly TColor DARKEST_AMBER = new TColor(63, 47, 0);
		public  static readonly TColor DARKEST_YELLOW = new TColor(63, 63, 0);
		public  static readonly TColor DARKEST_LIME = new TColor(47, 63, 0);
		public  static readonly TColor DARKEST_CHARTREUSE = new TColor(31, 63, 0);
		public  static readonly TColor DARKEST_GREEN = new TColor(0, 63, 0);
		public  static readonly TColor DARKEST_SEA = new TColor(0, 63, 31);
		public  static readonly TColor DARKEST_TURQUOISE = new TColor(0, 63, 47);
		public  static readonly TColor DARKEST_CYAN = new TColor(0, 63, 63);
		public  static readonly TColor DARKEST_SKY = new TColor(0, 47, 63);
		public  static readonly TColor DARKEST_AZURE = new TColor(0, 31, 63);
		public  static readonly TColor DARKEST_BLUE = new TColor(0, 0, 63);
		public  static readonly TColor DARKEST_HAN = new TColor(15, 0, 63);
		public  static readonly TColor DARKEST_VIOLET = new TColor(31, 0, 63);
		public  static readonly TColor DARKEST_PURPLE = new TColor(47, 0, 63);
		public  static readonly TColor DARKEST_FUCHSIA = new TColor(63, 0, 63);
		public  static readonly TColor DARKEST_MAGENTA = new TColor(63, 0, 47);
		public  static readonly TColor DARKEST_PINK = new TColor(63, 0, 31);
		public  static readonly TColor DARKEST_CRIMSON = new TColor(63, 0, 15);

		/* metallic */
		public  static readonly TColor BRASS = new TColor(191, 151, 96);
		public  static readonly TColor COPPER = new TColor(197, 136, 124);
		public  static readonly TColor GOLD = new TColor(229, 191, 0);
		public  static readonly TColor SILVER = new TColor(203, 203, 203);

		/* miscellaneous */
		public  static readonly TColor CELADON = new TColor(172, 255, 175);
		public  static readonly TColor PEACH = new TColor(255, 159, 127);
	}
}

