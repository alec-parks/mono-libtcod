﻿using System;

namespace TCOD
{
	public partial class THeightMap
	{
		internal TCOD_heightmap_t map;

		public THeightMap(int w, int h) {
			map = TCOD_heightmap_new(w, h);
		}

		~THeightMap() {
			TCOD_heightmap_delete(map);
		}

		public float GetValue(TPoint p) {
			return GetValue(p.x, p.y);
		}

		public float GetValue(int x, int y) {
			return TCOD_heightmap_get_value(map, x, y);
		}

		public float GetInterpolatedValue(float x, float y) {
			return TCOD_heightmap_get_interpolated_value(map, x, y);
		}

		public void SetValue(TPoint p, float value) {
			SetValue(p.x, p.y, value);
		}

		public void SetValue(int x, int y, float value) {
			TCOD_heightmap_set_value(map, x, y, value);
		}

		public float GetSlope(TPoint p) {
			return GetSlope(p.x, p.y);
		}

		public float GetSlope(int x, int y) {
			return TCOD_heightmap_get_slope(map, x, y);
		}

		public void GetNormal(float x, float y, float[] n, float waterLevel) {
			TCOD_heightmap_get_normal(map, x, y, n, waterLevel);
		}

		public int CountCells(float min, float max) {
			return TCOD_heightmap_count_cells(map, min, max);
		}

		public bool HasLandOnBordre(float waterLevel) {
			return TCOD_heightmap_has_land_on_border(map, waterLevel);
		}

		public void GetMinmax(ref float min, ref float max) {
			TCOD_heightmap_get_minmax(map, ref min, ref max);
		}

		public THeightMap Copy() {
			var copy = new THeightMap(map.w, map.h);
			TCOD_heightmap_copy(map, copy.map);
			return copy;
		}

		public void Add(float value) {
			TCOD_heightmap_add(map, value);
		}

		public void Scale(float value) {
			TCOD_heightmap_scale(map, value);
		}

		public void Clamp(float min, float max) {
			TCOD_heightmap_clamp(map, min, max);
		}

		public void Normalize(float min, float max) {
			TCOD_heightmap_normalize(map, min, max);
		}

		public void Clear() {
			TCOD_heightmap_clear(map);
		}

		static public THeightMap Lerp(THeightMap hm1, THeightMap hm2, float coef) {
			var hmres = new THeightMap(hm1.map.w, hm1.map.h);
			TCOD_heightmap_lerp_hm(hm1.map, hm2.map, hmres.map, coef);
			return hmres;
		}

		public THeightMap Add(THeightMap hm1, THeightMap hm2) {
			var hmres = new THeightMap(hm1.map.w, hm1.map.h);
			TCOD_heightmap_add_hm(hm1.map, hm2.map, hmres.map);
			return hmres;
		}

		public THeightMap Multiply(THeightMap hm1, THeightMap hm2) {
			var hmres = new THeightMap(hm1.map.w, hm1.map.h);
			TCOD_heightmap_multiply_hm(hm1.map, hm2.map, hmres.map);
			return hmres;
		}

		public void AddHill(float hx, float hy, float hradius, float hheight) {
			TCOD_heightmap_add_hill(map, hx, hy, hradius, hheight);
		}

		public void DigHill(float hx, float hy, float hradius, float hheight) {
			TCOD_heightmap_dig_hill(map, hx, hy, hradius, hheight);
		}

		public void DigBezier(int[] px, int[] py, float startRadius, float startDepth, float endRadius, float endDepth) {
			TCOD_heightmap_dig_bezier(map, px, py, startRadius, startDepth, endRadius, endDepth);
		}

		public void RainErosion(int nbDrops,float erosionCoef,float sedimentationCoef,TRandom rnd = null) {
			rnd = rnd ?? TRandom.GetInstance();
			TCOD_heightmap_rain_erosion(map, nbDrops, erosionCoef, sedimentationCoef, rnd.ptr);
		}

		public void KernelTransform(int[] dx, int[] dy, float[] weight, float minLevel,float maxLevel) {
			if (dx.Length == dy.Length && dy.Length == weight.Length) {
				TCOD_heightmap_kernel_transform(map, dx.Length, dx, dy, weight, minLevel, maxLevel);
			} else {
				throw new ArgumentException(string.Format("Size mismatch in array parameters: dx={0}, dy={1}, weight={2}", dx.Length, dy.Length, weight.Length));
			}
		}

		public void AddVornoi(int nbPoints, float[] coef, TRandom rnd) {
			rnd = rnd ?? TRandom.GetInstance();

			TCOD_heightmap_add_voronoi(map, nbPoints, coef.Length, coef, rnd.ptr);
		}

		public void MidPointDisplacement(TRandom rnd, float roughness) {
			rnd = rnd ?? TRandom.GetInstance();

			TCOD_heightmap_mid_point_displacement(map, rnd.ptr, roughness);
		}

		public void AddFBM(TNoise noise,float mulx, float muly, float addx, float addy, float octaves, float delta, float scale) {
			if (noise == null)
				throw new ArgumentException("cannot be null", "noise");

			TCOD_heightmap_add_fbm(map, noise.ptr, mulx, muly, addx, addy, octaves, delta, scale);
		}

		public void ScaleFBM(TNoise noise,float mulx, float muly, float addx, float addy, float octaves, float delta, float scale) {
			if (noise == null)
				throw new ArgumentException("cannot be null", "noise");

			TCOD_heightmap_scale_fbm(map, noise.ptr, mulx, muly, addx, addy, octaves, delta, scale);
		}

		public void Islandify(float seaLevel,TRandom rnd) {
			rnd = rnd ?? TRandom.GetInstance();

			TCOD_heightmap_islandify(map, seaLevel, rnd.ptr);
		}
	}
}

