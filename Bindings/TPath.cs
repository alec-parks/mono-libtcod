﻿using System;
using System.Collections.Generic;

namespace TCOD
{
	public partial class TPath
	{
		internal IntPtr ptr;

		public TPath(TMap map, float diagonalCost = 1.41f)
		{
			ptr = TCOD_path_new_using_map(map.ptr, diagonalCost);
		}

		public TPath(int width, int height, CostFunc func, IntPtr data, float diagonalCost)
		{
			ptr = TCOD_path_new_using_function(width, height, func, data, diagonalCost);
		}

		public bool Compute(TPoint o, TPoint d) {
			return TCOD_path_compute(ptr, o.x, o.y, d.x, d.y);
		}

		public bool Walk(ref TPoint p, bool recalculate_when_needed) {
			return TCOD_path_walk(ptr, ref p.x, ref p.y, recalculate_when_needed);
		}

		bool IsEmpty() {
			return TCOD_path_is_empty(ptr);
		}

		int Size() {
			return TCOD_path_size(ptr);
		}

		void Reverse() {
			TCOD_path_reverse(ptr);
		}

		public TPoint Get(int index) {
			TPoint p = new TPoint();
			TCOD_path_get(ptr, index, ref p.x, ref p.y);
			return p;
		}

		public TPoint Origin() {
			TPoint p = new TPoint();
			TCOD_path_get_origin(ptr, ref p.x, ref p.y);
			return p;
		}

		public TPoint Destination() {
			TPoint p = new TPoint();
			TCOD_path_get_destination(ptr, ref p.x, ref p.y);
			return p;
		}
	}

	public partial class TPathDijkstra
	{
		internal IntPtr ptr;
	}
}

