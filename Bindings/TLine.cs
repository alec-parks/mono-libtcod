﻿using System;
using System.Collections.Generic;

namespace TCOD
{
	public partial class TLine
	{

		public static IEnumerable<TPoint> Walk(TPoint p1, TPoint p2) {
			return Walk(p1.x, p1.y, p2.x, p2.y);
		}

		public static IEnumerable<TPoint> Walk(int xo, int yo, int xd, int yd) {
			TBresenhamData data = new TBresenhamData();
			TCOD_line_init_mt(xo, yo, xd, yd, data);

			TPoint step = new TPoint(xo, yo);
			do {
				yield return step;
			} while(!TCOD_line_step_mt(ref step.x, ref step.y, data));
		}
	}
}

