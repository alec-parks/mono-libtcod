﻿using System;

namespace TCOD
{
	public partial class TConsole
	{
		internal IntPtr ptr;

		private static TConsole _root;

		public static TConsole Root {
			get {
				if (_root == null)
					throw new Exception("Root console has not been initialized");
				return _root;
			}
		}

		public static TConsole InitRoot(int w, int h, string title, bool fullscreen) {
			IntPtr rootPtr = InitRoot(w, h, title, fullscreen, TRenderer.SDL);
			_root = new TConsole(rootPtr);
			return _root;
		}

		public void SetFade(byte val, TColor fade) {
			TConsole.SetFade(val, fade.val);
		}

		public TColor GetFadingColor() {
			var val = TConsole.GetFadingColorInt();
			return new TColor(val);
		}

		void SetColorControl(TColorControl con, TColor fore, TColor back) {
			TConsole.SetColorControl(con, fore.val, back.val);
		}

		public TConsole(int w, int h) {
			ptr = TConsole.CreateNew(w, h);
		}

		public TConsole(TSize size) {
			ptr = TConsole.CreateNew(size.w, size.h);
		}

		public TConsole(string filename) {
			ptr = TConsole.FromFile(filename);
		}

		private TConsole(IntPtr ptr) {
			this.ptr = ptr;
		}

		~TConsole() {
			TConsole.Delete(ptr);
		}

		public void SetDefaultBackground(TColor col) {
			TConsole.SetDefaultBackground(ptr, col.val);
		}


		public void SetDefaultForeground(TColor col) {
			TConsole.SetDefaultForeground(ptr, col.val);
		}


		public void Clear() {
			TConsole.Clear(ptr);
		}


		public void SetCharBackground(int x, int y, TColor col, TBackgroundFlag flag) {
			TConsole.SetCharBackground(ptr, x, y, col.val, flag);
		}

		public void SetCharForeground(int x, int y, TColor col) {
			TConsole.SetCharForeground(ptr, x, y, col.val);
		}


		public void SetChar(int x, int y, int c) {
			TConsole.SetChar(ptr, x, y, c);
		}

		public void PutChar(TPoint p, int c, TBackgroundFlag flag = TBackgroundFlag.None, TColor fg = null, TColor bg = null) {
			PutChar(p.x, p.y, c, flag, fg, bg);
		}

		public void PutChar(int x, int y, int c, TBackgroundFlag flag = TBackgroundFlag.None, TColor fg = null, TColor bg = null) {
			if (fg != null && bg != null) {
				TConsole.PutCharEx(ptr, x, y, c, fg.val, bg.val);
			} else {
				TConsole.PutCharStd(ptr, x, y, c, flag);
			}
		}

		public void Print(TPoint p, string fmt, params object[] args) {
			Print(p.x, p.y, fmt, args);
		}

		public void Print(int x, int y, string fmt, params object[] args) {
			TCOD_console_print(ptr, x, y, string.Format(fmt, args));
		}

		public void Print(int x, int y, TBackgroundFlag flag, TAlignment alignment, string fmt, params object[] args) {
			TCOD_console_print_ex(ptr, x, y, flag, alignment, string.Format(fmt, args));
		}

		public void Rect(int x, int y, int w, int h, bool clear, TBackgroundFlag flag = TBackgroundFlag.Set) {
			TConsole.Rect(ptr, x, y, w, h, clear, flag);
		}


		public void HLine(int x, int y, int l, TBackgroundFlag flag = TBackgroundFlag.None) {
			TConsole.HLine(ptr, x, y, l, flag);
		}

		public void VLine(int x, int y, int l, TBackgroundFlag flag = TBackgroundFlag.None) {
			TConsole.VLine(ptr, x, y, l, flag);
		}

		public void Frame(TRect rect, bool empty = false, TBackgroundFlag flag = TBackgroundFlag.None, string fmt = null, params string[] args) {
			TConsole.Frame(ptr, rect.origin.x, rect.origin.y, rect.size.w, rect.size.h, empty, flag, fmt != null ? string.Format(fmt, args) : null);
		}

		public void Frame(int x, int y, int w, int h, bool empty, TBackgroundFlag flag, string fmt = null, params string[] args) {
			TConsole.Frame(ptr, x, y, w, h, empty, flag, fmt != null ? string.Format(fmt, args) : null);
		}

		public void WithBackground(TColor bg, Action fn) {
			var oldBg = GetDefaultBackground();
			SetDefaultBackground(bg);
			fn();
			SetDefaultBackground(oldBg);
		}

		public void WithForeground(TColor fg, Action fn) {
			var oldFg = GetDefaultForeground();
			SetDefaultForeground(fg);
			fn();
			SetDefaultForeground(oldFg);
		}

		public TColor GetDefaultBackground() {
			var data = TConsole.GetDefaultBackground(ptr);
			return new TColor(data);
		}


		public TColor GetDefaultForeground() {
			var data = TConsole.GetDefaultForeground(ptr);
			return new TColor(data);
		}


		public TColor GetCharBackground(int x, int y) {
			var data = TConsole.GetCharBackground(ptr, x, y);
			return new TColor(data);
		}


		public TColor GetCharForeground(int x, int y) {
			var data = TConsole.GetCharForeground(ptr, x, y);
			return new TColor(data);
		}

		public int GetChar(int x, int y) {
			return TConsole.GetChar(ptr, x, y);
		}

		public bool LoadASC(string filename) {
			return TConsole.LoadASC(ptr, filename);
		}

		public bool LoadAPF(string filename) {
			return TConsole.LoadAPF(ptr, filename);
		}

		public bool SaveASC(string filename) {
			return TConsole.SaveASC(ptr, filename);
		}

		public bool SaveAPF(string filename) {
			return TConsole.SaveAPF(ptr, filename);
		}

		public TSize GetSize() {
			return new TSize {
				w = GetWidth(),
				h = GetHeight()
			};
		}

		public int GetWidth() {
			return TConsole.GetWidth(ptr);
		}

		public int GetHeight() {
			return TConsole.GetHeight(ptr);
		}

		public void SetKeyColor(TColor col) {
			TConsole.SetKeyColor(ptr, col.val);
		}

		public static void Blit(TConsole src, TRect srcRect, TConsole dst, TPoint dstPoint, float foreground_alpha = 1.0f, float background_alpha = 1.0f) {
			TConsole.Blit(src.ptr, srcRect.origin.x, srcRect.origin.y, srcRect.size.w, srcRect.size.h, dst.ptr, dstPoint.x, dstPoint.y, foreground_alpha, background_alpha);
		}

		public static void Blit(TConsole src, int xSrc, int ySrc, int wSrc, int hSrc, TConsole dst, int xDst, int yDst, float foreground_alpha = 1.0f, float background_alpha = 1.0f) {
			TConsole.Blit(src.ptr, xSrc, ySrc, wSrc, hSrc, dst.ptr, xDst, yDst, foreground_alpha, background_alpha);
		}
	}
}

