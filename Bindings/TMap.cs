﻿using System;

namespace TCOD
{
	public partial class TMap
	{
		internal IntPtr ptr;

		public TMap(int w, int h) {
			ptr = TCOD_map_new(w, h);
		}

		~TMap() {
			TCOD_map_delete(ptr);
		}

		public void Clear(bool transparent, bool walkable) {
			TCOD_map_clear(ptr, transparent, walkable);
		}

		public TMap Copy() {
			var w = GetWidth();
			var h = GetHeight();
			var copy = new TMap(w, h);
			TCOD_map_copy(ptr, copy.ptr);
			return copy;
		}

		public void SetProperties(int x, int y, bool is_transparent, bool is_walkable) {
			TCOD_map_set_properties(ptr, x, y, is_transparent, is_walkable);
		}

		public void ComputeFOV(int player_x, int player_y, int max_radius, bool light_walls, FovAlgorithm algo) {
			TCOD_map_compute_fov(ptr, player_x, player_y, max_radius, light_walls, algo);
		}

		public bool IsInFOV(int x, int y) {
			return TCOD_map_is_in_fov(ptr, x, y);
		}

		public void SetInFOV(int x, int y, bool fov) {
			TCOD_map_set_in_fov(ptr, x, y, fov);
		}

		public bool IsTransparent(int x, int y) {
			return TCOD_map_is_transparent(ptr, x, y);
		}

		public bool IsWalkable(int x, int y) {
			return TCOD_map_is_walkable(ptr, x, y);
		}

		public TSize GetSize() {
			return new TSize {
				w = GetWidth(),
				h = GetHeight()
			};
		}

		public int GetWidth() {
			return TCOD_map_get_width(ptr);
		}

		public int GetHeight() {
			return TCOD_map_get_height(ptr);
		}

		public int GetNbCells() {
			return TCOD_map_get_nb_cells(ptr);
		}
	}
}

