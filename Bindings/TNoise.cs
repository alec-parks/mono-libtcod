﻿using System;

namespace TCOD
{
	public partial class TNoise
	{
		internal IntPtr ptr;

		public TNoise(int dimensions, float hurst, float lacunarity, TRandom random) {
			ptr = TCOD_noise_new(dimensions, hurst, lacunarity, random.ptr);
		}

		~TNoise() {
			TCOD_noise_delete(ptr);
		}

		public void SetType(TNoiseType type) {
			TCOD_noise_set_type(ptr, type);
		}

		public float Get(float[] f, TNoiseType type) {
			return TCOD_noise_get_ex(ptr, f, type);
		}

		public float GetFBM(float[] f, float octaves, TNoiseType type) {
			return TCOD_noise_get_fbm_ex(ptr, f, octaves, type);
		}

		public float GetTurbulence(float[] f, float octaves, TNoiseType type) {
			return TCOD_noise_get_turbulence_ex(ptr, f, octaves, type);
		}

		public float Get(float[] f) {
			return TCOD_noise_get(ptr, f);
		}

		public float GetFBM(float[] f, float octaves) {
			return TCOD_noise_get_fbm(ptr, f, octaves);
		}

		public float GetTurbulence(float[] f, float octaves) {
			return TCOD_noise_get_turbulence(ptr, f, octaves);
		}
	}
}

