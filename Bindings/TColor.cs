﻿using System;

namespace TCOD
{
	public partial class TColor
	{
		internal TColorData val;

		public static TColor FromRGB(byte r, byte g, byte b) {
			return new TColor(r, g, b);
		}

		public static TColor FromHSV(float h, float s, float v) {
			var data = TColor.TCOD_color_HSV(h, s, v);
			return new TColor(data);
		}

		public static TColor Lerp(TColor c1, TColor c2, float coeff) {
			var data = TColor.TCOD_color_lerp(c1.val, c2.val, coeff);
			return new TColor(data);
		}

		public TColor(byte r, byte g, byte b) {
			val = TColor.TCOD_color_RGB(r, g, b);
		}

		internal TColor(TColorData val) {
			this.val = val;
		}

		public void SetHSV(float h, float s, float v) {
			TColor.TCOD_color_set_HSV(ref val, h, s, v);
		}

		public void GetHSV(out float h, out float s, out float v) {
			float tmpH = 0, tmpS = 0, tmpV = 0;
			TColor.TCOD_color_get_HSV(val, ref tmpH, ref tmpS, ref tmpV);
			h = tmpH;
			s = tmpS;
			v = tmpV;
		}

		public float GetHue() {
			return TColor.TCOD_color_get_hue(val);
		}

		public float GetSaturation() {
			return TColor.TCOD_color_get_saturation(val);
		}

		public float GetValue() {
			return TColor.TCOD_color_get_value(val);
		}

		public void SetHue(float h) {
			TColor.TCOD_color_set_hue(ref val, h);
		}

		public void SetSaturation(float s) {
			TColor.TCOD_color_set_saturation(ref val, s);
		}

		public void SetValue(float v) {
			TColor.TCOD_color_set_value(ref val, v);
		}

		public void ShiftHue(float shift) {
			TColor.TCOD_color_shift_hue(ref val, shift);
		}

		public void ScaleHSV(float scoef, float vcoef) {
			TColor.TCOD_color_scale_HSV(ref val, scoef, vcoef);
		}

		override public bool Equals(Object obj) {
			var other = obj as TColor;
			if (other == null)
				return false;
			
			return TColor.TCOD_color_equals(val, other.val);
		}

		override public int GetHashCode() {
			return val.r ^ val.g ^ val.b;
		}

		public static TColor operator+(TColor c1, TColor c2) {
			var data = TColor.TCOD_color_add(c1.val, c2.val);
			return new TColor(data);
		}

		public static TColor operator-(TColor c1, TColor c2) {
			var data = TColor.TCOD_color_subtract(c1.val, c2.val);
			return new TColor(data);
		}

		public static TColor operator*(TColor c1, TColor c2) {
			var data = TColor.TCOD_color_multiply(c1.val, c2.val);
			return new TColor(data);
		}

		public static TColor operator*(TColor c1, float v1) {
			var data = TColor.TCOD_color_multiply_scalar(c1.val, v1);
			return new TColor(data);
		}

	}
}

