﻿using System;

namespace TCOD
{
	public partial class TImage
	{
		internal IntPtr ptr;

		public TImage(int w, int h) {
			ptr = TCOD_image_new(w, h);
		}

		public TImage(string filename) {
			ptr = TCOD_image_load(filename);
		}

		public TImage(TConsole console) {
			ptr = TCOD_image_from_console(console.ptr);
		}

		~TImage() {
			TCOD_image_delete(ptr);
		}

		public void RefreshConsole(TConsole console) {
			TCOD_image_refresh_console(ptr, console.ptr);
		}

		public void Clear(TColor color) {
			TCOD_image_clear(ptr, color.val);
		}

		public void Invert() {
			TCOD_image_invert(ptr);
		}

		public void HFlip() {
			TCOD_image_hflip(ptr);
		}

		public void Rotate90(int numRotations) {
			TCOD_image_rotate90(ptr, numRotations);
		}

		public void VFlip() {
			TCOD_image_vflip(ptr);
		}

		public void Scale(int neww, int newh) {
			TCOD_image_scale(ptr, neww, newh);
		}

		public void Save(string filename) {
			TCOD_image_save(ptr, filename);
		}

		public void GetSize(ref int w, ref int h) {
			TCOD_image_get_size(ptr, ref w, ref h);
		}

		public TColor GetPixel(int x, int y) {
			var col = TCOD_image_get_pixel(ptr, x, y);
			return new TColor(col);
		}

		public int GetAlpha(int x, int y) {
			return TCOD_image_get_alpha(ptr, x, y);
		}

		public TColor GetMipmapPixel(float x0, float y0, float x1, float y1) {
			var col = TCOD_image_get_mipmap_pixel(ptr, x0, y0, x1, y1);
			return new TColor(col);
		}

		public void PutPixel(int x, int y, TColor col) {
			TCOD_image_put_pixel(ptr, x, y, col.val);
		}

		public void Blit(TConsole console, float x, float y, TBackgroundFlag bkgnd_flag, float scalex, float scaley, float angle) {
			TCOD_image_blit(ptr, console.ptr, x, y, bkgnd_flag, scalex, scaley, angle);
		}

		public void BlitRect(TConsole console, int x, int y, int w, int h, TBackgroundFlag bkgnd_flag) {
			TCOD_image_blit_rect(ptr, console.ptr, x, y, w, h, bkgnd_flag);
		}

		public void Blit2x(TConsole dest, int dx, int dy, int sx, int sy, int w, int h) {
			TCOD_image_blit_2x(ptr, dest.ptr, dx, dy, sx, sy, w, h);
		}

		public void SetKeyColor(TColor key_color) {
			TCOD_image_set_key_color(ptr, key_color.val);
		}

		public bool IsPixelTransparent(int x, int y) {
			return TCOD_image_is_pixel_transparent(ptr, x, y);
		}
	}
}

